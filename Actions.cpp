/* Actions.cpp
 * This is where you should define the
 * behavior of your actions. Make sure
 * the parameters match those in the JSON
 * exactly! Double check Extension.h
 * as well.
 */

#include "Common.h"
#include "StringHelpers.h"
#include "JSONHelpers.h"
#include "ScopeStack.h"


void Extension::LoadJSONString(TCHAR const * jsonStr)
{
	auto newObj = parseToTJson(stdtstring(jsonStr), this->lastError);

	// don't continue with the replace if parse failed
	if (lastError.empty())
	{
		// have to bust the cache because we might be replacing anything
		// KDTODO: Can potentially examine the path and only clear what's needed
		scopeStack->getScope().applyAndClearCache();
		scopeStack->getScope().set(_T("$"), std::move(newObj), this->lastError);
	}
}

void Extension::LoadJSONStringAtPath(TCHAR const * path, TCHAR const * jsonStr)
{
	auto newObj = parseToTJson(stdtstring(jsonStr), this->lastError);

	// don't continue with the replace if parse failed
	if (lastError.empty())
	{
		// have to bust the cache because we might be replacing anything
		// KDTODO: Can potentially examine the path and only clear what's needed
		scopeStack->getScope().applyAndClearCache();
		scopeStack->getScope().set(path, std::move(newObj), this->lastError);
	}
}

void Extension::ClearError()
{
	this->lastError = _T("");
}


void Extension::SetString(TCHAR const * path, TCHAR const * val)
{
	scopeStack->getScope().set(path, tjson::make_string(val), this->lastError);
}

void Extension::SetFloat(TCHAR const * path, float val)
{
	scopeStack->getScope().set(path, tjson::from_floating_point(val), this->lastError);
}

void Extension::SetInt(TCHAR const * path, int val)
{
	scopeStack->getScope().set(path, tjson::from_integer(val), this->lastError);
}

void Extension::CreateData(TCHAR const * path)
{
	scopeStack->getScope().set(path, tjson::object(), this->lastError);
}

void Extension::CreateArray(TCHAR const * path)
{
	scopeStack->getScope().set(path, tjson::array(), this->lastError);
}

void Extension::AppendString(TCHAR const * path, TCHAR const * val)
{
	scopeStack->getScope().append(path, tjson::make_string(val), this->lastError);
}
void Extension::AppendFloat(TCHAR const * path, float val)
{
	scopeStack->getScope().append(path, tjson::from_floating_point(val), this->lastError);
}
void Extension::AppendInt(TCHAR const * path, int val)
{
	scopeStack->getScope().append(path, tjson::from_integer(val), this->lastError);
}
void Extension::AppendObject(TCHAR const * path)
{
	scopeStack->getScope().append(path, tjson::object(), this->lastError);
}
void Extension::AppendArray(TCHAR const * path)
{
	scopeStack->getScope().append(path, tjson::array(), this->lastError);
}

void Extension::InsertString(TCHAR const * path, int insertAt, TCHAR const * val)
{
	scopeStack->getScope().insertAt(path, insertAt, tjson::make_string(val), this->lastError);
}
void Extension::InsertFloat(TCHAR const * path, int insertAt, float val)
{
	scopeStack->getScope().insertAt(path, insertAt, tjson::from_floating_point(val), this->lastError);
}
void Extension::InsertInt(TCHAR const * path, int insertAt, int val)
{
	scopeStack->getScope().insertAt(path, insertAt, tjson::from_integer(val), this->lastError);
}
void Extension::InsertObject(TCHAR const * path, int insertAt)
{
	scopeStack->getScope().insertAt(path, insertAt, tjson::object(), this->lastError);
}
void Extension::InsertArray(TCHAR const * path, int insertAt)
{
	scopeStack->getScope().insertAt(path, insertAt, tjson::array(), this->lastError);
}

void Extension::RemoveAt(TCHAR const * path, int removeAt)
{
	scopeStack->getScope().removeAt(path, removeAt, this->lastError);
}

void Extension::SetStringAt(TCHAR const * path, int setAt, TCHAR const * val)
{
	scopeStack->getScope().setAt(path, setAt, tjson::make_string(val), this->lastError);
}
void Extension::SetFloatAt(TCHAR const * path, int setAt, float val)
{
	scopeStack->getScope().setAt(path, setAt, tjson::from_floating_point(val), this->lastError);
}
void Extension::SetIntAt(TCHAR const * path, int setAt, int val)
{
	scopeStack->getScope().setAt(path, setAt, tjson::from_integer(val), this->lastError);
}
void Extension::SetObjectAt(TCHAR const * path, int setAt)
{
	scopeStack->getScope().setAt(path, setAt, tjson::object(), this->lastError);
}
void Extension::SetArrayAt(TCHAR const * path, int setAt)
{
	scopeStack->getScope().setAt(path, setAt, tjson::array(), this->lastError);
}


void Extension::SetScope(TCHAR const * path)
{
	stdtstring pathStr = path;
	auto scopeObj = scopeStack->getScope().getFirst(pathStr, this->lastError);
	if (lastError.empty())
	{
		// pass along ownership of the json object to the scope (keeps it on the stack for the lifetime of the scope)
		scopeStack->setScope(std::move(scopeObj), pathStr);
	}
}
void Extension::SetAbsoluteScope(TCHAR const * path)
{
	this->lastError = _T("SetAbsoluteScope is deprecated.");
}
void Extension::UndoScope()
{
	scopeStack->undoScope(this->lastError);
}
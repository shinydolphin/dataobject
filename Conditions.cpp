/* Conditions.cpp
 * This is where you should define the
 * behavior of your conditions. Make sure
 * the parameters and return types match those
 * in the JSON exactly! Double check Extension.h
 * as well.
 */

#include "Common.h"
#include "JSONHelpers.h"
#include "ScopeStack.h"

bool Extension::PathExists(TCHAR const * path)
{
	return scopeStack->getScope().exists(path, this->lastError);
}

bool Extension::HasError()
{
	return !this->lastError.empty();
}

bool Extension::IsArray(TCHAR const * path)
{
	return scopeStack->getScope().getFirst(path, this->lastError).is_array();
}

bool Extension::IsData(TCHAR const * path)
{
	return scopeStack->getScope().getFirst(path, this->lastError).is_object();
}

bool Extension::IsString(TCHAR const * path)
{
	return scopeStack->getScope().getFirst(path, this->lastError).is_string();
}

bool Extension::IsFloat(TCHAR const * path)
{
	return scopeStack->getScope().getFirst(path, this->lastError).is_double();
}

bool Extension::IsInt(TCHAR const * path)
{
	return scopeStack->getScope().getFirst(path, this->lastError).is_integer();
}

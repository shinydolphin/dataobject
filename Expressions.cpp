/* Expressions.cpp
 * This is where you should define the
 * behavior of your expressions. Make sure
 * the parameters and return types match those
 * in the JSON exactly! Double check Extension.h
 * as well.
 */

#include "Common.h"
#include "JSONHelpers.h"
#include "StringHelpers.h"
#include "ScopeStack.h"

TCHAR const * Extension::StrVal(TCHAR const * path, TCHAR const * defaultVal)
{
	auto result = scopeStack->getScope().getFirstCached(path, this->lastError);

	if (result.is_string())
	{
		return Runtime.CopyString(result.as_string().c_str());
	}
	else
	{
		return Runtime.CopyString(defaultVal);
	}
}

float Extension::FloatVal(TCHAR const * path, float defaultVal)
{
	auto result = scopeStack->getScope().getFirstCached(path, this->lastError);

	if (result.is_number())
	{
		return (float)result.as_double();
	}
	else
	{
		return defaultVal;
	}
}

int Extension::IntVal(TCHAR const * path, int defaultVal)
{
	auto result = scopeStack->getScope().getFirstCached(path, this->lastError);

	if (result.is_number())
	{
		return result.as_int();
	}
	else
	{
		return defaultVal;
	}
}

TCHAR const * Extension::JSONPath(TCHAR const * path)
{
	return Runtime.CopyString(scopeStack->getScope().getJSONFirst(path, this->lastError, true).c_str());
}

TCHAR const * Extension::JSONQuery(TCHAR const * path)
{
	return Runtime.CopyString(scopeStack->getScope().getJSONAll(path, this->lastError, true).c_str());
}

TCHAR const * Extension::JSONError()
{
	return Runtime.CopyString(this->lastError.c_str());
}

TCHAR const * Extension::JSONStr()
{
	return JSONPath(_T("$"));
}
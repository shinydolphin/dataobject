#ifndef _HASHTABLE_H_
#define _HASHTABLE_H_

#include "StringHelpers.h"
#include <vector>

#define DEFAULT_TABLE_SIZE			89
#define	DEFAULT_BUCKET_CAPACITY		10
#define COMPRESSION_A				3
#define	COMPRESSION_B				7


// KDTODO: Move to some utils file
// wrappers for deleting and setting the pointer to null
#define		SAFE_DELETE(ptr)	{ if(ptr) { delete ptr; ptr = NULL; } }
#define		SAFE_DELETE_ARR(ptr)	{ if(ptr) { delete[] ptr; ptr = NULL; } }


// struct holding a (key, data) pair
template <typename T>
struct HashTableEntry
{
	TCHAR * key;
	T data;

	HashTableEntry()
	{
	}

	HashTableEntry(TCHAR * key, const T & data)
	{
		this->key = key;
		this->data = data;
	}
};

/* Array based hash table with c string keys, using some TCH character type (char, wchar, or TCHAR for ex)
*/
template <typename T>
class HashTable
{
private:

	// the actual array
	std::vector<std::vector< HashTableEntry<T>>> data;

	// number of buckets in the hash table
	int tableSize;

	// number of actual elements currently stored in the table
	int count;

	// helper functions
	unsigned int hashFunc(TCHAR const * key, int length) const;
	unsigned int secondaryHash(unsigned int primaryHash) const;
	int findKeyInBucket(TCHAR const * key, int bucket) const;

public:

	// constructor/destructor
	HashTable(int tableSize = DEFAULT_TABLE_SIZE);
	virtual ~HashTable();

	// KDTODO: Move constructor, assignment, copy

	// add and remove elements
	void add(TCHAR const * key, const T & element);
	void remove(TCHAR const * key);
	void clear();

	// modify existing elements
	void set(TCHAR const * key, const T & element);
	void addOrSet(TCHAR const * key, const T & element);

	// basic access
	T & get(TCHAR const * key);
	const T & get(TCHAR const * key) const;
	T & operator[](TCHAR const * key);
	const T & operator[](TCHAR const * key) const;

	// get size info
	int getCount() const;

	// search functions
	bool contains(TCHAR const * key) const;

};


//------------
// implementation
//------------

// constructor
template <typename T> HashTable<T>::HashTable(int tableSize)
{
	// create the array
	this->tableSize = tableSize;
	this->count = 0;

	// create an empty list in each bucket with the desired capacity
	for (int i = 0; i < tableSize; i++)
	{
		// vector constructor actually constructions N entries, need to call reserve to set the capacity
		std::vector<HashTableEntry<T>> bucket;
		bucket.reserve(DEFAULT_BUCKET_CAPACITY);
		data.push_back(std::move(bucket));
	}
}

// destructor
template <typename T> HashTable<T>::~HashTable()
{
	// clear to clean up owned strings
	clear();
}

template <typename T> unsigned int HashTable<T>::hashFunc(TCHAR const * key, int size) const
{
	// cycle shift hash code
	unsigned int hash = 0;
	for (int i = 0; i < size; i++)
	{
		hash = (hash << 5) | (hash >> 27);
		hash += (unsigned int)key[i];
	}
	return hash;
}

// compress an integer (already sent through primary hash function) to the table size 
template <typename T> unsigned int HashTable<T>::secondaryHash(unsigned int primaryHash) const
{
	return (COMPRESSION_A * primaryHash + COMPRESSION_B) % tableSize;
}

// add an element to the list (don't try and add same key twice, it'll only give the first one)
template <typename T> void HashTable<T>::add(TCHAR const * key, const T & element)
{
	// make an internally managed copy of the key that we can store in the hashtableentry and delete later
	int size = _tcslen(key) + 1;
	TCHAR * ownedKey = new TCHAR[size];
	_tcscpy_s(ownedKey, size, key);

	// get hash of the key and map it to table size
	unsigned int keyHash = secondaryHash(hashFunc(key, sizeof(ownedKey)));

	// put the new element at the end
	data[keyHash].push_back(HashTableEntry<T>(ownedKey, element));
	count++;
}

// finds the element given and removes it
template <typename T> void HashTable<T>::remove(TCHAR const * key)
{
	// get hash of the key and map it to table size
	unsigned int keyHash = secondaryHash(hashFunc(key, sizeof(key)));
	int pos = findKeyInBucket(key, keyHash);

	// free the stored key that we own
	SAFE_DELETE_ARR(data[keyHash].at(pos).key);

	// remove the (key, entry) pair from the bucket
	data[keyHash].erase(data[keyHash].begin() + pos);
	count--;
}

// remove all elements from the list
template <typename T> void HashTable<T>::clear()
{
	for (int i = 0; i < tableSize; i++)
	{
		// free the stored keys that we own
		for (unsigned int pos = 0; pos < data[i].size(); pos++)
		{
			SAFE_DELETE_ARR(data[i].at(pos).key);
		}

		data[i].clear();
	}
	count = 0;
}

template <typename T> void HashTable<T>::set(TCHAR const * key, const T & element)
{
	// get hash of the key and map it to table size
	unsigned int keyHash = secondaryHash(hashFunc(key, sizeof(key)));
	int pos = findKeyInBucket(key, keyHash);

	data[keyHash].at(pos).data = element;
}

template <typename T> void HashTable<T>::addOrSet(TCHAR const * key, const T & element)
{
	if (contains(key))
	{
		set(key, element);
	}
	else
	{
		add(key, element);
	}
}

// returns the element at given index
template <typename T> const T & HashTable<T>::get(TCHAR const * key) const
{
	// get hash of the key and map it to table size
	unsigned int keyHash = secondaryHash(hashFunc(key, sizeof(key)));
	int pos = findKeyInBucket(key, keyHash);

	return data[keyHash].at(pos).data;
}

// returns the element at given index
template <typename T> T & HashTable<T>::get(TCHAR const * key)
{
	// get hash of the key and map it to table size
	unsigned int keyHash = secondaryHash(hashFunc(key, sizeof(key)));
	int pos = findKeyInBucket(key, keyHash);

	return data[keyHash].at(pos).data;
}

// overloaded brackets
template <typename T> T & HashTable<T>::operator[](TCHAR const * key)
{
	return get(key);
}

// overloaded brackets 
template <typename T> const T & HashTable<T>::operator[](TCHAR const * key) const
{
	return get(key);
}

// returns true if the key exists in the hash table
template <typename T> bool HashTable<T>::contains(TCHAR const * key) const
{
	// get hash of the key and map it to table size
	unsigned int keyHash = secondaryHash(hashFunc(key, sizeof(key)));
	int pos = findKeyInBucket(key, keyHash);

	// return false if key does not exist in table
	return !(keyHash < 0 || keyHash >= (unsigned int)tableSize || pos < 0 || (unsigned int)pos >= data[keyHash].size());
}

// get the number of (key, element) pairs in the hash table
template <typename T> int HashTable<T>::getCount() const
{
	return count;
}

// KDTODO: Make an iterator so we can access everything in the hash table and apply it
//template<typename T> 

// get the index in the bucket that a key is in, -1 if not there
template <typename T> int HashTable<T>::findKeyInBucket(TCHAR const * key, int bucket) const
{
	// find the index in the bucket
	for (unsigned int i = 0; i < data[bucket].size(); i++)
	{
		bool equal = _tcscmp(data[bucket].at(i).key, key) == 0;
		if (equal)
		{
			return i;
		}
	}

	return -1;
}

#endif
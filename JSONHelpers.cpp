#include "JSONHelpers.h"
#include "StringHelpers.h"
#include <regex>

using namespace jsoncons;
using namespace jsoncons::jsonpath;

static const stdtstring finalizeJsonStr(const stdtstring & jsonStr)
{
	// convert all single quotes to double quotes
	auto convertedSingleQuotes = std::regex_replace(jsonStr, tregex(_T("'")), _T("\""));

	// Match a letter or an underscore. Then match zero or more characters, in which each may be a digit, a letter, or an underscore.
	// This excludes any keys that are already quoted because they will have a quote in between the last character and the :
	// The letters and the : are in separate match groups so we can include those in the replace string
	// Based on http://www.informit.com/articles/article.aspx?p=2079020
	auto keyRegex = _T("([a-zA-Z_][a-zA-Z_0-9]*)(:)");

	// add quotes around any keys that aren't already surrounded by quotes ($1 is the key, $2 is the :)
	auto autoQuotedKeys = std::regex_replace(convertedSingleQuotes, tregex(keyRegex), _T("\"$1\"$2"));

	return autoQuotedKeys;
}

const stdtstring finalizePathStr(const stdtstring & path)
{
	int cappedLen = _tcsnlen(path.c_str(), 1);

	// conversion cases we want to support:
	// 1) Empty String -> defaults to root
	// 2) first -> $.first
	// 3) [0] -> $[0]
	if (cappedLen == 0)
	{
		return _T("$");
	}
	else if (path[0] != _T('$') && path[0] != _T('['))
	{
		stdtstring result = _T("$.");
		result += path;
		return result;
	}
	else if (path[0] == _T('['))
	{
		stdtstring result = _T("$");
		result += path;
		return result;
	}
	else
	{
		return path;
	}
}

static int getLastDeref(const stdtstring & path)
{
	// size_t is unsigned, need to convert to an int so we get -1 as invalid
	int lastBracket = path.find_last_of(_T("["));
	int lastDot = path.find_last_of(_T("."));

	// return the index of the final  before the last dereference
	int lastDeref = lastDot > lastBracket ? lastDot : lastBracket;
	
	return lastDeref;
}

bool isSimplePath(const stdtstring & path)
{
	// Forbidden characters:
	// @ * , : ( ) ? * / + - & | = ! < > ~ -
	// From https://github.com/danielaparker/jsoncons/blob/master/doc/ref/jsonpath/jsonpath.md
	for (unsigned int i = 0; i < path.length(); i++)
	{
		// this is more efficient than a regex, and performance is critical since this is used in cache checks
		if (path[i] == _T('@') ||
			path[i] == _T('*') ||
			path[i] == _T(',') ||
			path[i] == _T(':') ||
			path[i] == _T('(') ||
			path[i] == _T(')') ||
			path[i] == _T('?') ||
			path[i] == _T('*') ||
			path[i] == _T('/') ||
			path[i] == _T('+') ||
			path[i] == _T('-') ||
			path[i] == _T('&') ||
			path[i] == _T('|') ||
			path[i] == _T('=') ||
			path[i] == _T('!') ||
			path[i] == _T('<') ||
			path[i] == _T('>') ||
			path[i] == _T('~') ||
			path[i] == _T('-')
			)
		{
			return false;
		}
	}

	return true;
}

const stdtstring getBeforeLastDeref(const stdtstring & path)
{
	return path.substr(0, getLastDeref(path));
}

const stdtstring getAfterLastDeref(const stdtstring & path)
{
	// for ex. "store.books[0].price" (size 20, last dot at index 15) ... before . is "store.books[0]" ... after . is "price"
	int lastDeref = getLastDeref(path);
	return path.substr(lastDeref + 1, path.length() - lastDeref);
}

// the jsoncons lib requires stdtstrings so it's best if you convert to one somewhere outside if you need to do it somewhere too
tjson jsonpath_query(const tjson & root, const stdtstring & path, stdtstring & error)
{
	tjson result;
	try
	{
		result = json_query(root, finalizePathStr(path));
	}
	catch (const parse_error & e)
	{
		result = tjson();
		error = charToTCharStr(e.what());
	}

	return result;
}
	
// the jsoncons lib requires stdtstrings so it's best if you convert to one somewhere outside if you need to do it somewhere too
tjson jsonpath_first(const tjson & root, const stdtstring & path, stdtstring & error)
{
	auto result = jsonpath_query(root, path, error);

	// select the first result, or return tjson's null variant if we got no results back
	if (result.is_array() && !result.is_empty())
	{
		// this returns a reference to a json object within our original object
		// can't do result = result[0] because the original result object goes out of scope,
		// invalidating the reference to result[0] that we had. returning immediately makes a hard copy since this
		// function doesn't return by reference, so that solves it
		return result[0];
	}

	return tjson::null();
}

// the jsoncons lib requires stdtstrings so it's best if you convert to one somewhere outside if you need to do it somewhere too
void jsonpath_replaceall(tjson & root, const stdtstring & path, const tjson & newValue, stdtstring & error)
{
	tjson result;
	try
	{
		json_replace(root, finalizePathStr(path), newValue);
	}
	catch (const parse_error & e)
	{
		error = charToTCharStr(e.what());
	}
}

stdtstring tjsonToString(const tjson & obj, bool pretty)
{
	tostringstream os;

	if (pretty)
	{
		os << pretty_print(obj) << std::endl;
	}
	else
	{
		os << print(obj) << std::endl;
	}

	return os.str();
}

tjson parseToTJson(stdtstring & jsonStr, stdtstring & error)
{
  tjson result;

  try
  {
		strict_parse_error_handler err_handler;
		result = tjson::parse(finalizeJsonStr(jsonStr), err_handler);
  }
  catch (const parse_error& e)
  {
		// use {} as the default when we have an error
		result = tjson();
		error = charToTCharStr(e.what());
  }

  return result;
}

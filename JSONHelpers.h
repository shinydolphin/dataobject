#pragma once

#include <jsoncons/json.hpp>
#include <jsoncons_ext/jsonpath/json_query.hpp>

#include "StringHelpers.h"

// use the correct version of jsoncons based on unicode or not
#ifdef UNICODE
  typedef jsoncons::wojson tjson;
#else
  typedef jsoncons::ojson tjson;
#endif


bool isSimplePath(const stdtstring & path);
const stdtstring finalizePathStr(const stdtstring & path);
const stdtstring getBeforeLastDeref(const stdtstring & path);
const stdtstring getAfterLastDeref(const stdtstring & path);
stdtstring tjsonToString(const tjson & obj, bool pretty);
tjson parseToTJson(stdtstring & jsonStr, stdtstring & error);
tjson jsonpath_query(const tjson & root, const stdtstring & path, stdtstring & error);
tjson jsonpath_first(const tjson & root, const stdtstring & path, stdtstring & error);
void jsonpath_replaceall(tjson & root, const stdtstring & path, const tjson & newValue, stdtstring & error);
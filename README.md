# README #

This is a Clickteam Fusion 2.5 extension that lets you create hierarchical nested data structures of strings, floats, and ints and query them using JSONPath syntax. 

The goal is to make it possible to write more complex code in Fusion, and to bridge the gap for programmers who are frustrated with Fusion's limited data structures. You can make stacks, queues, linked lists, trees, graphs, dictionaries, and nest all of those within each other with low effort.

There are a few other extensions that give you hierarchical data structures. The most similar being the [Structured Data Object](http://www.lb-stuff.com/MMF2/Extensions/). I feel that the Data Object is significantly easier to use for me, though this may come down to personal preference. Data Object happens to fit my coding style better, and I imagine others may find it useful. 

### Performance ###

JSONCons and JSONPath are not terribly fast. You're only going to notice a problem if you are doing hundreds of Data Object calls a frame, but if you're doing some complex algorithms it can become an issue. To battle this, Data Object has a simple caching layer. Learning to avoid cache busting is important if you are hitting major bottlenecks.

Structured Data Object is probably faster since it doesn't use JSON as a base but I haven't confirmed this.

### Basic Usage ###

Suppose you have "Lockers" in your game, and each locker can store up to 10 items. Each item can have a name and an upgrade level. We can represent a single locker with the following data structure (an array of objects):

[
	{name: "sword", level: 3},
	{name: "dagger", level: 2},
	{name: "shield", level: 5}
]

In Fusion, we often might use a List object for this, with the data encoded as a string (for ex. "sword|3"), but it's troublesome to access it because we have to do substrings. Further, we have multiple lockers. We'd need a separate list object per locker.

Instead, we can represent all lockers in this data structure using the Fixed Value of the locker objects to identify them in the data:

{
	lockers: {
		"2341432": [
			{name: "sword", level: 3},
			{name: "dagger", level: 2},
			{name: "shield", level: 5}
		],
		"9832153": [
			{name: "gun", level: 8}
		],
		"1182749": [
			{name: "dagger", level: 9},
			{name: "sword", level: 1}
		]
	}
}

Now, let's see what we can do with this using JSONPath queries:

* Get Value at Path "lockers["9832153"][0].name" - Gets the name of the first item in the locker with Fixed Value 9832153
* Get Value at Path "lockers["2341432"].length" - Gets the number of items in the locker with Fixed Value 2341432
* Set Scope "lockers["1182749"][0]" - Changes your view of the data to be rooted at the first item in 1182749, allowing the following to work...
* Get Value at Path "level" - Gives you the level of the sword, 1

### Using Scope for Recursion ###

The scope system is very powerful. It can be used to mimick the behavior of a call stack. If you adhere to some simple "calling conventions," you can write complex recursive algorithms. It should be possible to transcribe anything written in a normal language to Fusion events using this pattern. For example, Polyroll uses DataObject to write a recursive Depth First Search in the MAP screen.

### Other Info ###

The DataObject is released under the Boost license, which is the same license as the C++ implementation of jsonpath that it uses, jsoncons.

The DataObject is Copyright 2018 Kevin Dressel and Shiny Dolphin Games
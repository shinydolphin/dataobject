#include "ScopeStack.h"

#include "Common.h"
#include "StringHelpers.h"
#include "JSONHelpers.h"

using namespace std;

// make this sufficiently large to avoid collisions in typical uses of the cache, keeping operations to O(1)
const int BUCKET_COUNT = 89;

////////////////////////////////////////////////////
// Scope
////////////////////////////////////////////////////

AbstractScope::AbstractScope(const stdtstring & path, AbstractScope * relativeTo) :
	Path(path),
	RelativeToScope(relativeTo),
	Cache(std::make_unique<HashTable<tjson>>(BUCKET_COUNT))
{
}

const tjson & AbstractScope::ObjConst() const
{
	return const_cast<AbstractScope *>(this)->Obj();
}

RootScope::RootScope(tjson & obj) :
	AbstractScope(_T(""), NULL),
	objRef(obj)
{
}

tjson & RootScope::Obj()
{
	return objRef;
}

Scope::Scope(tjson obj, const stdtstring & path, AbstractScope * relativeTo) :
	AbstractScope( path, relativeTo),
	scopeObj(std::move(obj))
{
}

tjson & Scope::Obj()
{
	return scopeObj;
}

bool AbstractScope::exists(const stdtstring & path, stdtstring & error) const
{
	// getFirst returns tjson::null when there's no results found for the path query
	return !getFirst(path, error).is_null();
}

tjson AbstractScope::getFirstCached(TCHAR const * path, stdtstring & error) const
{
	if (!Cache->contains(path))
	{
		Cache->add(path, getFirst(path, error));
	}
	return Cache->get(path);
}

tjson AbstractScope::getFirst(const stdtstring & path, stdtstring & error) const
{
	return jsonpath_first(this->ObjConst(), path, error);
}

tjson AbstractScope::getAll(const stdtstring & path, stdtstring & error) const
{
	return jsonpath_query(this->ObjConst(), path, error);
}

void AbstractScope::replace(const stdtstring & path, tjson val, stdtstring & error)
{
	// replace the path in the local scope object. when undoScope is called, it will be applied to parent scopes
	jsonpath_replaceall(this->Obj(), path, val, error);
}

void AbstractScope::applyAndClearCache()
{
	Cache->clear();
}

void AbstractScope::set(const stdtstring & path, tjson val, stdtstring & error)
{
	// if the path is simple (no dereferencing or fancy operators), we can just update the cache entry instead of busting it
	if (isSimplePath(path))
	{
		Cache->addOrSet(path.c_str(), val);
	}
	else
	{
		applyAndClearCache();
	}

	// KDTODO: I could make it hold off on actually modifying the json if it's a simple path because we can just update the cache
	// then next time the cache is busted or we access something that's not in the cache we will have to run through all the cache entries
	// and run replace on them then.

	// convert to a finalized path so we don't have to do it multiple times deeper down
	stdtstring finalPath = finalizePathStr(path);

	if (exists(finalPath, error))
	{
		replace(finalPath, val, error);
	}
	else
	{
		// KDTODO: Review compatibility with more complex JSON path queries and paths that are designed to return multiple results
		// pretty sure all my assumptions break if you use any more complex features
		// use something similar to isSimplePath (but allowing [, ], and .) to complain if user is trying to set something that doesn't exist with a complex path
		const stdtstring beforeLastDeref = getBeforeLastDeref(finalPath);
		const stdtstring afterLastDeref = getAfterLastDeref(finalPath);

		// you can't auto-add array indices, only object keys, so check we're an object first
		// if that passes, we know that the last deref wasn't a bracket, must have been a dot, so we can assume that from now on
		// this also fails if the target object doesn't exist (is_object will fail), since we can't recursively containers that don't exist
		tjson targetObj = getFirst(beforeLastDeref, error);
		if (error.empty() && targetObj.is_object())
		{
			targetObj[afterLastDeref] = val;

			// finally, apply the change by replacing the entire destination object in the root object's hierarchy
			replace(beforeLastDeref, targetObj, error);
		}
		else
		{
			error = beforeLastDeref + _T(" doesn't exist. Cannot create new field on it until you create it first.\n") + error;
		}
	}
}

// KDTODO: This is a little tricky. We need to do something like what we did with set where there's some limitations
// Do a for loop over each result and call the remove/set code on each entry. That's the way to do it.
// then the only limitation is the last part of the path HAS to be a simple string key (so it can be added/removed)
// this seems reasonable since you are doing a set/get operation. Maybe can add an error if it's anything other than a simple string (look at finalize)
void AbstractScope::remove(const stdtstring & path, stdtstring & error)
{
	error = _T("Remove not implemented yet.");
}

// KDTODO: This way of handling errors is stupid. use exceptions instead
void AbstractScope::append(const stdtstring & path, tjson val, stdtstring & error)
{
	// too complicated to do anything clever here, just bust the cache
	applyAndClearCache();

	// finalize the path here so we don't have to do it twice
	stdtstring finalPath = finalizePathStr(path);

	auto result = getFirst(finalPath, error);
	if (result.is_array() && error.empty())
	{
		result.push_back(val);
		replace(finalPath, result, error);
	}
}

void AbstractScope::insertAt(const stdtstring & path, int index, tjson val, stdtstring & error)
{
	// too complicated to do anything clever here, just bust the cache
	applyAndClearCache();

	// finalize the path here so we don't have to do it twice
	stdtstring finalPath = finalizePathStr(path);

	// KDTODO: Try taking in a uint index directly instead of converting it to uint like below
	auto result = getFirst(finalPath, error);
	if (result.is_array() && (uint)index < result.size() + 1 && error.empty())
	{
		result.insert(result.array_value().begin() + index, val);
		replace(finalPath, result, error);
	}
	else
	{
		tostringstream err;
		err << _T("Could not insert at ") << index << _T(" because ") << path << _T(" is not long enough. Max index you can insert at is " << result.size()) << _T("\n") << error;
		error = err.str();
	}
}

void AbstractScope::setAt(const stdtstring & path, int index, tjson val, stdtstring & error)
{
	// too complicated to do anything clever here, just bust the cache
	applyAndClearCache();

	// finalize the path here so we don't have to do it twice
	stdtstring finalPath = finalizePathStr(path);

	// jsoncons doesn't seem to have a set at iterator function for arrays, so we mimic it with a remove and insert
	// but still don't allow you to set an array index without adding elements. you can't do a set_or_add with an array.
	auto result = getFirst(finalPath, error);
	if (result.is_array() && (uint)index < result.size() && error.empty())
	{
		removeAt(finalPath, index, error);
		insertAt(finalPath, index, val, error);
	}
	else
	{
		tostringstream err;
		err << _T("Could not set at ") << index << _T(" because ") << path << _T(" the index doesn't exist. Length is ") << result.size() << _T("\n") << error;
		error = err.str();
	}
}

void AbstractScope::removeAt(const stdtstring & path, int removeAt, stdtstring & error)
{
	// too complicated to do anything clever here, just bust the cache
	applyAndClearCache();

	// finalize the path here so we don't have to do it twice
	stdtstring finalPath = finalizePathStr(path);

	auto result = getFirst(finalPath, error);
	if (result.is_array() && (uint)removeAt < result.size() && error.empty())
	{
		result.erase(result.array_value().begin() + removeAt);
		replace(finalPath, result, error);
	}
	else
	{
		tostringstream err;
		err << _T("Could not remove at ") << removeAt << _T(" because ") << path << _T(" is not long enough.\n") << error;
		error = err.str();
	}
}

stdtstring AbstractScope::getDebugPathDump() const
{
	tostringstream result;
	auto idx = 1;
	auto curr = this;
	while (curr->RelativeToScope != NULL)
	{
		result << idx << ":" << curr->Path << _T("  \n");
		curr = curr->RelativeToScope;
		idx++;
	}

	return result.str();
}

stdtstring AbstractScope::getJSONFirst(const stdtstring & path, stdtstring & error, bool pretty) const
{
	return tjsonToString(getFirst(path, error), pretty);
}

stdtstring AbstractScope::getJSONAll(const stdtstring & path, stdtstring & error, bool pretty) const
{
	return tjsonToString(getAll(path, error), pretty);
}



////////////////////////////////////////////////////
// Scope Stack
////////////////////////////////////////////////////

ScopeStack::ScopeStack(tjson & rootObj)
{
	this->root = unique_ptr<AbstractScope>(new RootScope(rootObj));
}

// retrieve the current scope
bool ScopeStack::isScoped() const
{
	return !stack.empty();
}

AbstractScope & ScopeStack::getScope()
{
	if (isScoped())
	{
		return *stack.top();
	}
	else
	{
		return *root;
	}
}

// backs up to the previous scope, unless we've already undone everything (isScoped would have returned false)
void ScopeStack::undoScope(stdtstring & error)
{
	if (isScoped())
	{
		// Need to apply changes from the current scope down to the hierarchy below it because we're about to access parent scope data
		if (stack.top()->RelativeToScope != NULL)
		{
			stack.top()->RelativeToScope->replace(stack.top()->Path, stack.top()->Obj(), error);
		}

		stack.pop();
	}
}

// sets a new scope relative to the current scope. use undoScope to recover previous scope.
void ScopeStack::setScope(tjson scopeObj, const stdtstring & scopePath)
{
	// no need to apply any changes to parent hierarchy because none of that is accessible
	// relative to root if the stack is empty, or relative to the top of the stack otherwise
	AbstractScope * relativeTo = root.get();
	if (!stack.empty())
	{
		relativeTo = stack.top().get();
	}

	stack.push(unique_ptr<AbstractScope>( new Scope(std::move(scopeObj), finalizePathStr(scopePath), relativeTo) ));
}

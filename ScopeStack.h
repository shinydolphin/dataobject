#pragma once

#include <memory>
#include <stack>

#include "HashTable.h"
#include "StringHelpers.h"
#include "JSONHelpers.h"

class AbstractScope
{
protected:
	std::unique_ptr<HashTable<tjson>> Cache;
	
public:
	AbstractScope * RelativeToScope;
	stdtstring Path;

	AbstractScope(const stdtstring & path, AbstractScope * relativeTo);
	virtual ~AbstractScope() {}

	// tjson has a quirk/bug that makes it impossible to heap allocate and persist once it goes out of scope
	// so now, every time you need to access a scope (other than root, which persists through the extension)
	// this function will create a fresh json_query result and return it
	virtual tjson & Obj() = 0;
	const tjson & ObjConst() const;

	bool exists(const stdtstring & path, stdtstring & error) const;
	tjson getFirstCached(TCHAR const * path, stdtstring & error) const;
	tjson getFirst(const stdtstring & path, stdtstring & error) const;
	tjson getAll(const stdtstring & path, stdtstring & error) const;

	void set(const stdtstring & path, tjson val, stdtstring & error);
	void replace(const stdtstring & path, tjson val, stdtstring & error);
	void remove(const stdtstring & path, stdtstring & error);

	void applyAndClearCache();

	// array ops
	void append(const stdtstring & path, tjson val, stdtstring & error);
	void insertAt(const stdtstring & path, int index, tjson val, stdtstring & error);
	void removeAt(const stdtstring & path, int removeAt, stdtstring & error);
	void setAt(const stdtstring & path, int index, tjson val, stdtstring & error);

	stdtstring getJSONFirst(const stdtstring & path, stdtstring & error, bool pretty) const;
	stdtstring getJSONAll(const stdtstring & path, stdtstring & error, bool pretty) const;
	stdtstring getDebugPathDump() const;
};

// this type of scope takes in a ref to the root object owned by the Extension class instance. this never goes out of scope
class RootScope : public AbstractScope
{
	tjson & objRef;

public:
	RootScope(tjson & obj);
	tjson & Obj();
};

// this type of scope creates a fresh tjson view every single time it needs one, doing a new json_query from the path every time.
class Scope : public AbstractScope
{
	tjson scopeObj;

public:
	Scope(tjson obj, const stdtstring & path, AbstractScope * relativeTo);
	tjson & Obj();
};

class ScopeStack
{
	std::unique_ptr<AbstractScope> root;
	std::stack<std::unique_ptr<AbstractScope>> stack;

public:

	ScopeStack(tjson & rootObj);

	bool isScoped() const;
	AbstractScope & getScope();

	// backs up to the previous scope, unless we've already undone everything (isScoped would have returned false)
	void undoScope(stdtstring & error);

	// sets a new scope relative to the current scope. use undoScope to recover previous scope.
	void setScope(tjson obj, const stdtstring & scopePath);
};
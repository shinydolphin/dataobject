#pragma once

#include <string>
#include <iostream>
#include <memory>
#include "Common.h"

#include "StringHelpers.h"

using namespace std;

/**
* Take a char based string and return a NEW wchar_t based string.
* @param	origString	The string to copy into a wide string
* @return				A wide string that is a copy of origString
*/
static const wstring createWString(const char *origString)
{
	int size = strlen(origString) + 1;
	wchar_t *buffer = new wchar_t[size];
	mbstowcs(buffer, origString, size);

	wstring result = buffer;
	delete[] buffer;

	return result;
}

/**
* Take a wchar_t based string and return a NEW char based string.
* The caller is responsible for deleting it.
* @param	origString	The wide string to copy into a byte string
* @return				A byte string that is a copy of origString
*/
static const string createBString(const wchar_t *origString)
{
	int size = wcslen(origString) + 1;
	char *buffer = new char[size];
	wcstombs(buffer, origString, size);

	string result = buffer;
	delete[] buffer;

	return result;
}

// For working with windows api's tchar, which is defined either as char or wchar_t depending on whether UNICODE is defined

const stdtstring charToTCharStr(const char *origString)
{
#ifdef  UNICODE
	return createWString(origString);
#else
	return origString;
#endif
}

const string tCharToCharStr(const TCHAR * origString)
{
#ifdef  UNICODE
	return createBString(origString);
#else
	return origString;
#endif
}
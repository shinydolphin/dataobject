#pragma once

#include <string>
#include <iostream>
#include <regex>
#include <tchar.h>

// KD: Copied here from EDIF's Common.h so that you don't have to include common to get it
/* stdtstring
* A std::string that knows if you're using
* unicode or not. (Protip: strings passed
* to your extension A/C/Es still need to be
* const TCHAR *, and the same goes for strings
* returned from expressions and conditions).
*/
typedef std::basic_string<TCHAR> stdtstring;


// For working with windows api's tchar, which is defined either as char or wchar_t depending on whether UNICODE is defined
const stdtstring charToTCharStr(const char *origString);
const std::string tCharToCharStr(const TCHAR * origString);


// TCHAR stringstream
#ifdef UNICODE
typedef std::wostringstream tostringstream;
#else
typedef std::ostringstream tostringstream;
#endif

// TCHAR regex
#ifdef UNICODE
typedef std::wregex tregex;
#else
typedef std::regex tregex;
#endif
